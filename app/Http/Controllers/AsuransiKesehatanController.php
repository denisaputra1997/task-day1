<?php

namespace App\Http\Controllers;

use App\Models\AsuransiKesehatan;
use App\Http\Requests\StoreAsuransiRequest;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;


class AsuransiKesehatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = AsuransiKesehatan::get();
        return Inertia::render('Askes', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAsuransiRequest $request)
    {
        $asu = AsuransiKesehatan::where('no_ktp', $request->no_ktp)->first();
        if($asu) {
            return Redirect::route('asuransi_kesehatan')->with([
                'message' => 'Nomor KTP '.$request->no_ktp.' gagal disimpan karena sudah ada!',
                'type' => 'error',
            ]);

        }
        AsuransiKesehatan::create($request->all());
        return Redirect::route('asuransi_kesehatan')->with(['message' => 'data berhasil di input!', 'type' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AsuransiKesahatan  $asuransiKesahatan
     * @return \Illuminate\Http\Response
     */
    public function show(AsuransiKesehatan $asuransiKesehatan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AsuransiKesahatan  $asuransiKesahatan
     * @return \Illuminate\Http\Response
     */
    // public function edit(AsuransiKesehatan $asuransiKesehatan)
    // {
    //     return Inertia::render('Askes', ['askes' => $asuransiKesehatan]);
    // }
    public function edit($id)
    {
        $askes = AsuransiKesehatan::where('id', $id)->first();
        return Inertia::render('Askes', compact('askes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AsuransiKesahatan  $asuransiKesahatan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // $asuransiKesehatan->fill($request->all());
        $asu = AsuransiKesehatan::findOrFail($request->id);
        $asu->update($request->all());
        return Redirect::route("asuransi_kesehatan")->with(['message' => 'data terupdate!', 'type' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AsuransiKesahatan  $asuransiKesahatan
     * @return \Illuminate\Http\Response
     */
    public function destroy(AsuransiKesehatan $asuransiKesehatan)
    {
        //
    }
}
