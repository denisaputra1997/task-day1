<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
// use Illuminate\Validation\Validator;


class StoreAsuransiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_ktp'                => 'required|min:16|numeric',
            'nama_lengkap'          => 'required',
            'jenis_kelamin'         => 'required',
            'tempat_lahir'          => 'required',
            'tanggal_lahir'         => 'required',
            'status_perkawinan'     => 'required',
            'no_hp'                 => 'required',
            'npwp'                  => 'required',
            'kewarganegaraan'       => 'required',
            'kelas'                 => 'required',
            'email'                 => 'required',
            'alamat'                => 'required',
            'rt'                    => 'required',
            'rw'                    => 'required',
            'kode_pos'              => 'required',
            'no_tlp_rumah'          => 'required',
            'kelurahan'             => 'required',
            'no_kk'                 => 'required',
            'status_dlm_keluarga'   => 'required',
            // 'jumlah_anak'           => 'required',
            'rekening'              => 'required',
            'nomor_rek'             => 'required',
            'nama_rek'              => 'required',
        ];
        // return [
        //     'no_ktp'                => 'require|numeric|min:16',
        //     'nama_lengkap'          => 'require|string',
        //     'jenis_kelamin'         => 'require|string',
        //     'tempat_lahir'          => 'require|string',
        //     'tanggal_lahir'         => 'require|date',
        //     // 'status_pernikahan'     => 'require|string',
        //     // 'no_hp'                 => 'require|numeric',
        //     // 'npwp'                  => 'require|numeric',
        //     // 'kewarganegaraan'       => 'require|string',
        //     // 'kelas'                 => 'require|string',
        //     // 'email'                 => 'require|email',
        //     // 'alamat'                => 'require|string',
        //     // 'rt'                    => 'require|string',
        //     // 'rw'                    => 'require|string',
        //     // 'kode_pos'              => 'require|numeric',
        //     // 'no_tlp_rumah'          => 'require|numeric',
        //     // 'kelurahan'             => 'require|string',
        //     // 'no_kk'                 => 'require|numeric',
        //     // 'status_dlm_keluarga'   => 'require|string',
        //     // 'jumlah_anak'           => 'require|numeric',
        //     // 'rekening'              => 'require|numeric',
        //     // 'nomor_rek'             => 'require|numeric',
        //     // 'nama_rek'              => 'require|string',
        // ];
    }
}
