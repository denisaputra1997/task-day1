# Training Al - Amin

-   git clone https://gitlab.com/banabda/training-alamin.git
-   cp .env.exampe .env
-   composer install
-   npm install
-   npm run dev
-   php artisan migrate
-   php artisan serve

## Day 1 - Laravel & Vue basic

-   Using Controller
-   Using Inertia
-   Using Routes
-   Using Models
-   Create form in Vue with tailwind
-   CRUD Laravel Vue Inertia
-   Using sweet alert
-   Using Vue components

### Task Day 1

-   [Form Example](https://hafizhfikhi.web.ugm.ac.id/wp-content/uploads/sites/403/2015/05/form1.png)
-   Add KTP number checker
-   If 'Status Dalam Keluarga' 'Seorang Diri', column 'Jumlah Anak' disabled
-   Add edit features
