<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\AsuransiKesehatanController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

use App\Http\Controllers\TestingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');

    Route::get('/testing', [TestingController::class, 'index']);
    Route::resource('product', ProductController::class);
    Route::resource('asuransi_kesehatan', AsuransiKesehatanController::class);
    Route::get('asuransi_kesehatan/{id}/edit', [AsuransiKesehatanController::class, 'edit'])->name('editAskes');
    Route::put('asuransi_kesehatan/{id}/edit', [AsuransiKesehatanController::class, 'update'])->name('update');
    Route::get('asuransi_kesehatan', [AsuransiKesehatanController::class, 'index'])->name('asuransi_kesehatan');

    // Route::get('edit', [AsuransiKesehatanController::class, 'edit'])->name('editAskes');
});


require __DIR__ . '/auth.php';
