<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AsuransiKesehatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asuransi_kesehatans', function (Blueprint $table) {
            $table->id();
            $table->integer('no_ktp')->strengh(16)->nullable();
            $table->string('nama_lengkap')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('status_perkawinan')->nullable();
            $table->integer('no_hp')->nullable();
            $table->integer('npwp')->nullable();
            $table->string('kewarganegaraan')->nullable();
            $table->string('kelas')->nullable();
            $table->string('email')->nullable();
            $table->string('alamat')->nullable();
            $table->string('rt')->nullable();
            $table->string('rw')->nullable();
            $table->integer('kode_pos')->nullable();
            $table->string('no_tlp_rumah')->nullable();
            $table->string('kelurahan')->nullable();
            $table->integer('no_kk')->nullable();
            $table->string('status_dlm_keluarga')->nullable();
            $table->integer('jumlah_anak')->nullable();
            $table->integer('rekening')->nullable();
            $table->string('nomor_rek')->nullable();
            $table->string('nama_rek')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
